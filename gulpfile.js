let spawn = require('child_process').spawn;
let gulp = require('gulp');
let ts = require('gulp-typescript');
let merge = require('merge2');
let mocha = require('gulp-mocha');
let node;

let tsProject = ts.createProject('tsconfig.json');

gulp.task('build', function() {
    let tsResult = tsProject.src()
        .pipe(tsProject());

    return merge([ // Merge the two output streams, so this task is finished when the IO of both operations is done.
        tsResult.js.pipe(gulp.dest('dist'))
    ]);
});

gulp.task('run', ['build'], function() {
  if (node) {
      node.kill();
  }
  node = spawn('node', ['dist/index.js'], {stdio: 'inherit'})
  node.on('close', function (code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });
});

gulp.task('watch', ['run'], function() {
    gulp.watch('src/**/*.ts', ['run']);
});

gulp.task('test', ['build'], () => {
    gulp.src('dist/spec/*.spec.js')
        .pipe(mocha({
            reporter: 'nyan'
        }))
})

gulp.task('tdd', ['test'], () => {
    gulp.watch(['spec/**/*.ts', 'src/**/*.ts'], ['test'])
});

process.on('exit', function() {
    if (node) node.kill();
});