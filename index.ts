import { Configuration } from './src/configuration';
import { Worker } from './src/worker';
import * as io from 'socket.io-client';
import * as jwt from 'jsonwebtoken';
import * as mongoose from 'mongoose';
import * as http from 'http';
import * as bluebird from 'bluebird';

let token = jwt.sign({ workerId: 'StatsWorker' }, Configuration.secret, {
    expiresIn: '24h'
});

mongoose.connect(Configuration.challenger_node_connection_string);

global.Promise = bluebird;
(<any>mongoose).Promise = global.Promise;

let worker = new Worker();

let client = io(`${Configuration.notificationUri}?token=${token}`);

client.on('connect', async () => {
    console.log('connected...');
    await worker.scan([]);
});

client.on('scan', async (usernames: string[]) => {
    console.log('scanning...');
    await worker.scan(usernames);
});