export class Configuration {
    static worker_port = 6002;
    static db_host_port: number = 27017;
    static db_user: string = 'challenger-node-db';
    static db_pwd: string = 'r7X06EzywXT6j$!^K9AAJ2eH';
    static challenger_node_connection_string: string = `mongodb://${Configuration.db_user}:${Configuration.db_pwd}@localhost/challenger-node`;
    static challenger_node_connection_test_string: string = `mongodb://localhost/challenger-node-test`;
    static notificationUri: string = process.env.node_env === 'production' ? 'http://localhost:6001' : 'http://localhost:6001';
    static secret: string = 'asdlikhwqkmdvnoqsfhqwfdaspfjasf';
}