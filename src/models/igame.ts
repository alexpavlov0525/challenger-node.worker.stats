import * as mongoose from 'mongoose';

export interface IGame extends mongoose.Document {
    dateCreated: Date;
    dateStarted: Date;
    dateFinished: Date;
    name: string;
    templateId: string;
    winner: string;
    rounds: any[];
    players: string[];
    readyPlayers: string[];
    status: string;
    scanned: string[];
}