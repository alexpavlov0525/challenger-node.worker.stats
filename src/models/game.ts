import { Document, Schema, Model, model} from 'mongoose';
import {IGame} from './igame';

export interface IGameModel extends IGame, Document {
}

export let GameSchema: Schema = new Schema({
    dateCreated: Date,
    dateStarted: Date,
    dateFinished: Date,
    name: String,
    templateId: String,
    winner: String,
    rounds: Array,
    status: String,
    players: [String],
    readyPlayers: [String],
    scanned: [String]
});

GameSchema.pre('save', next => {
  next();
});

export const Game: Model<IGameModel> = model<IGameModel>('Game', GameSchema);