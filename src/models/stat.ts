import { Document, Schema, Model, model} from 'mongoose';
import {IStat} from './istat';

export interface IStatModel extends IStat, Document {
}

export let StatSchema: Schema = new Schema({
    dateCreated: Date,
    dateUpdated: Date,
    username: String,
    templates: Array,
});

StatSchema.pre('save', next => {
  next();
});

export const Stat: Model<IStatModel> = model<IStatModel>('Stat', StatSchema);