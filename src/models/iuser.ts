import * as mongoose from 'mongoose';

export interface IUser extends mongoose.Document {
    dateCreated: Date;
    lastLoggedIn: Date;
    hash: string;
    salt: string;
    username: string;
    roles: string[];
}