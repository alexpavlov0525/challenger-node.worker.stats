import * as mongoose from 'mongoose';

export interface ITemplate extends mongoose.Document {
    name: string;
    description: string;
    createdBy: string;
    minPlayers: number;
    maxPlayers: number;
    rounds: number;
    roundsToWin: number;
    dateCreated: Date;
    lastEdited: Date
}