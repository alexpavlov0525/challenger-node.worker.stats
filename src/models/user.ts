import { Document, Schema, Model, model} from 'mongoose';
import {IUser} from './iuser';

export interface IUserModel extends IUser, Document {
}

export let UserSchema: Schema = new Schema({
    dateCreated: Date,
    lastLoggedIn: Date,
    hash: String,
    salt: String,
    roles: [String],
    username: String,
});

UserSchema.pre('save', next => {
  next();
});

export const User: Model<IUserModel> = model<IUserModel>('User', UserSchema);