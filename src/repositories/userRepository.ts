import { User } from './../models/user';

export class UserRepository {
    async getAll() {
        return await User.find();
    }
}