import { Template } from './models/template';
import { User, UserSchema } from './models/user';
import { Game } from './models/game';
import { Stat } from './models/stat';
import { Configuration } from './configuration';

export class Worker {
    async getUsers() {
        let users = await User.find().select({ username: 1 });
        return users.map(x => x.username);
    }

    createNewStat(username: string) {
        return new Stat({
            dateCreated: new Date(),
            dateUpdated: new Date(),
            username: username,
            templates: []
        });
    }

    async getGamesFor(username: string) {
        return await Game.find({ players: username, scanned: { '$ne': username } });
    }

    async createTemplateStat(templateId: string) {
        let template = await Template.findById(templateId);
        let templateStat = {
            templateName: template.name,
            templateId: template._id,
            wonCount: 0,
            lostCount: 0,
            percentage: 0,
            roundsWon: 0,
            roundsLost: 0,
            globalRating: 0
        };
        return templateStat;
    }

    getUniqueTemplates(collection: any[]) {
        let arr: any[] = [];
        collection.forEach(x => {
            if (!arr.find(y => y === x.templateId)) {
                arr.push(x.templateId);
            }
        });
        return arr;
    }

    async populateStatTemplates(newTemplateIds: any[], existingTemplates: any[]): Promise<any[]> {
        for (let i = 0; i < newTemplateIds.length; i++) {
            if (!existingTemplates.find(x => String(x.templateId) === String(newTemplateIds[i]))) {
                let templateStat = await this.createTemplateStat(newTemplateIds[i]);
                existingTemplates.push(templateStat);
            }
        }
        return existingTemplates;
    }

    updateGamesWonLostCounts(game: any, template: any, username: string) {
        if (game.winner) {
            if (game.winner === username) {
                template.wonCount += 1;
            } else {
                template.lostCount += 1;
            }
        }
    }

    updateRoundsWonLostCounts(game: any, template: any, username: string) {
        for (let i3 = 0; i3 < game.rounds.length; i3++) {
            if (game.rounds[i3].winner) {
                if (game.rounds[i3].winner === username) {
                    template.roundsWon += 1;
                } else {
                    template.roundsLost += 1;
                }
            }
        }
    }

    async scan(usernames: string[]) {
        if (usernames.length === 0) {
            usernames = await this.getUsers();
        }

        console.log('scanning usernames: ', usernames);

        for (let index = 0; index < usernames.length; index++ ) {
            let stat = await Stat.findOne({ username: usernames[index] });
            if (!stat) {
                stat = this.createNewStat(usernames[index]);
            }

            let games = await this.getGamesFor(usernames[index]);

            let uniqueTemplateIds = this.getUniqueTemplates(games);

            stat.templates = await this.populateStatTemplates(uniqueTemplateIds, stat.templates);

            if (games.length > 0) {
                for (let i = 0; i < stat.templates.length; i++) {
                    let gamesOfTemplate = games.filter(x => String(x.templateId) === String(stat.templates[i].templateId));

                    if (gamesOfTemplate.length > 0) {
                        for (let i2 = 0; i2 < gamesOfTemplate.length; i2++) {
                            this.updateGamesWonLostCounts(gamesOfTemplate[i2], stat.templates[i], usernames[index]);
                            this.updateRoundsWonLostCounts(gamesOfTemplate[i2], stat.templates[i], usernames[index]);
                            gamesOfTemplate[i2].scanned.push(usernames[index]);
                            await gamesOfTemplate[i2].save();
                        }
                        stat.templates[i].percentage 
                            = (stat.templates[i].wonCount/(stat.templates[i].wonCount + stat.templates[i].lostCount)).toFixed(2);
                    }
                }
                stat.markModified('templates');
                await stat.save();
                let savedStat = await Stat.findById(stat._id);
            }
        }
    }
}