import { Stat } from './../src/models/stat';
import { Template } from './../src/models/template';
import { Game } from './../src/models/game';
import { User } from './../src/models/user';
import { Configuration } from './../src/configuration';
import { Worker } from './../src/worker';
import { suite, test } from 'mocha-typescript';
import { expect } from 'chai';
import * as mongoose from 'mongoose';
import 'sinon';

@suite
class WorkerTest {
    worker: Worker = new Worker();
    readonly username1: string = 'user1';
    readonly username2: string = 'user2';
    readonly templateName1: string = 'theGame1';
    readonly templateName2: string = 'theGame2';

    static async before() {
        mongoose.connect(Configuration.challenger_node_connection_test_string);

        let user1 = new User({
            username: 'user1'
        });
        await user1.save();

        let user2 = new User({
            username: 'user2'
        });
        await user2.save();

        let template1 = new Template({
            name: 'theGame1'
        });
        let savedTemplate1 = await template1.save();

        let template2 = new Template({
            name: 'theGame2'
        });
        let savedTemplate2 = await template2.save();

        for (let i = 0; i < 10; i++) {
            let template = this.getRandomIntInclusive(0,1) === 1 
                ? savedTemplate1 : savedTemplate2;
            let game = new Game({
                name: template.name,
                templateId: template._id,
                status: 'Finished',
                players: ['user1', 'user2'],
                winner: this.getRandomIntInclusive(0,1) === 1 ? 'user1' : 'user2'

            })
            await game.save();
        }

        let game = new Game({
                name: savedTemplate2.name,
                templateId: savedTemplate1._id,
                status: 'Finished',
                players: ['user1', 'user2'],
                winner: 'user1'
        });
        let games = await Game.find({});
    }

    static getRandomIntInclusive(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }


    static async after() {
        await User.remove({});
        await Game.remove({});
        await Stat.remove({});
        await Template.remove({});
        mongoose.disconnect();
    }

    @test('should init')
    public init() {
        expect(this.worker).to.not.equal(undefined);
    }

    @test('can get users')
    public async canGetUsers() {
        let users = await this.worker.getUsers();
        expect(users).to.not.equal(null);
        expect(users.length).to.equal(2);
    }

    @test('can create new stat')
    public canCreateNewStat() {
        let newStat = this.worker.createNewStat(this.username1);
        expect(newStat).to.not.equal(null);
        expect(newStat.username).to.equal(this.username1);
    }

    @test('can get games for user1')
    public async canGetGames() {
        let games = await this.worker.getGamesFor(this.username1);
        expect(games.length).to.be.greaterThan(0);
    }

    @test('can create a template stat')
    public async canCreateTemplateStat() {
        let template = await Template.findOne({name: this.templateName1});
        let templateStat = await this.worker.createTemplateStat(template._id) as any;
        expect(templateStat.templateName).to.equal(this.templateName1);
    }

    @test('can populate templates array on a stat that already has one item in it')
    public async canPopulateTemplatesOnAStat() {
        
        let games = await this.worker.getGamesFor(this.username1);
        let newtemplates = this.worker.getUniqueTemplates(games);
        let stat = this.worker.createNewStat(this.username1);
        let newTemplate = await Template.findOne({name: this.templateName1});
        let newTemplateStat = await this.worker.createTemplateStat(newTemplate._id);
        stat.templates = [];
        stat.templates = await this.worker.populateStatTemplates(newtemplates, stat.templates);

        expect(stat.templates.length).to.equal(2);
        expect(stat.templates.filter(x => x.templateName === this.templateName1).length)
            .to.equal(1);
        expect(stat.templates.filter(x => x.templateName === this.templateName2).length)
            .to.equal(1);            
    }

    @test('can get unique templates')
    public async canGetUniqueValues() {
        let games = await this.worker.getGamesFor(this.username1);
        let templates = this.worker.getUniqueTemplates(games);
        expect(templates.length).to.equal(2);
    }

    @test('can update games won and lost')
    public async canUpdateGamesCount() {
        let games = await this.worker.getGamesFor(this.username1);
        let template = {
            wonCount: 0,
            lostCount:0
        };
        games.forEach(game => {
            this.worker.updateGamesWonLostCounts(game, template, this.username1);
        });
        expect(template.wonCount).to.be.greaterThan(0);
    }

    @test('can scan')
    public async canScan() {
        await this.worker.scan([this.username1, this.username2]);
        let stats = await Stat.find();
        expect(stats[0].templates[0].wonCount).to.be.greaterThan(0);
    }
}